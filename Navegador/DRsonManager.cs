using System;
using System.IO;

namespace Navegador
{
    class DRsonManager
    {

        public CachePage GetPage(string direccion)
        {
            string[] text = File.ReadAllLines("cache.drson");
            int numLine = 0;
            foreach (string line in text)
            {
                if (line.StartsWith("😋") && line.EndsWith("😈"))            //Solo entra si es inicio de bloque
                {
                    string url = "";
                    for (int pos = 2; pos < (line.Length - 2); pos++)
                    {
                        url += line[pos];
                    }
                    if (url.Equals(direccion))
                    {
                        string html = this.getHTMLPageCode(text[numLine + 1]);
                        aumentarUsoAPagina(line, text);
                        return html != null ? new CachePage(html, true) : null;
                    }
                }
                numLine++;
            }
            return null;
        }
        //
        //Aumenta uso a pagina espesificada.
        //
        private void aumentarUsoAPagina(string pageLine, string[] textoArchivo)
        {
            int cont = 0;
            foreach (string line in textoArchivo)
            {
                if (line.Equals(pageLine))
                {
                    textoArchivo[cont + 2] = (int.Parse(textoArchivo[cont + 2]) + 1).ToString();
                    File.WriteAllLines("cache.drson", textoArchivo);
                    return;
                }
                cont++;
            }
        }

        public bool SavePage(string url, CachePage pagina)
        {
            if (cachedPagesCount() < 30)                                         //Revisa que no halla exeso de paginas en el índice.
            {
                try
                {
                    String[] text = File.ReadAllLines("cache.drson");
                    Array.Resize<String>(ref text, text.Length + 3);
                    text[text.Length - 3] = "😋" + url + "😈";                         //Pagina al indice
                    string name;
                    do
                    {
                        name = Path.GetRandomFileName() + ".ch";               //Busca nombre aleatorio y prueba que no exista otro igual
                    } while (File.Exists("CachePages/" + name));
                    text[text.Length - 2] = name;
                    text[text.Length - 1] = "1";
                    using(StreamWriter writer = File.CreateText("CachePages/" + name))
                    {
                        writer.Write(pagina.Html);
                    }
                    modifiedCachedCount(ref text, 1);
                    File.WriteAllLines("cache.drson", text);                  //Si todo sale bien agrega datos al índice.
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                Delete(this.GetMenosUsada());                                  //Elimina la menos usada
                return SavePage(url, pagina);                                  //Vuelve a intentar guardar (recursivo)
            }
        }

        public bool Delete(string pagina)
        {
            try
            {
                string[] text = File.ReadAllLines("cache.drson");
                int textPos = Array.IndexOf(text, ("😋" + pagina + "😈"));  //Revisión de existencia de página en indice.
                if (textPos != -1)                                           //-1 indica que no está ahí
                {
                    try
                    {
                        File.Delete("CachePages/" + text[textPos + 2]);
                    }
                    catch (Exception)
                    {
                        //No se que poner aqui :v
                    }
                    string[] toWrite = new string[text.Length - 3];          //Esestima campos que se van a elimar.
                    int textPosition = 0;
                    int writePosition = 0;
                    foreach (string line in text)
                    {
                        if (textPosition != textPos && textPosition != textPos + 1 && textPosition != textPos + 2)
                        {
                            toWrite[writePosition] = line;                   //Solo copia dato si no está en campos a eliminar.
                            writePosition++;
                        }
                        textPosition++;
                    }
                    modifiedCachedCount( ref toWrite, -1);
                    File.WriteAllLines("cache.drson", toWrite);             //Reescribe archivo con nuevo array resultante.
                    return true;
                }
                return false;

            }
            catch (Exception)
            {
                return false;
            }
        }

        //
        //Devulve número paginas en caché.
        //
        public int cachedPagesCount()
        {
            string[] text = File.ReadAllLines("cache.drson");
            string firstLine = text[0];
            foreach (char caracter in firstLine)
            {
                try
                {
                    int count = int.Parse(caracter.ToString());
                    return count;
                }
                catch (Exception)
                {
                    continue;
                }
            }
            return -1;
        }

        private string getHTMLPageCode(string path)
        {
            try
            {
                return File.ReadAllText("CachePages/" + path);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private string GetMenosUsada()
        {
            try
            {
                string selected = null;
                short menor = short.MaxValue;
                int pos = 0;
                string[] text = File.ReadAllLines("cache.drson");
                foreach (string line in text)
                {
                    if (line.StartsWith("😋") && line.EndsWith("😈"))
                    {
                        if (short.Parse(text[pos + 2]) < menor)
                        {
                            selected = "";
                            menor = short.Parse(text[pos + 2]);
                            for (int i = 2; i < line.Length - 2; i++) {
                                selected += line[i];
                            }
                        }
                    }
                    pos++;
                }
                return selected;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void modifiedCachedCount( ref string[] text, int cambio)
        {
            try
            {
                text[0] = "😎page num = " + (cachedPagesCount() + cambio).ToString() + "🙊";
                File.WriteAllLines("cache.drson", text);
            }
            catch (Exception)
            {
                //No se que poner aqui tampoco :v
            }
        }
    }

}
