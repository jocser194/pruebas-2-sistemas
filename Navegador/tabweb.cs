﻿using System;
using System.Windows.Forms;

namespace Navegador
{
    public partial class Tabweb : UserControl
    {
        private bool FromCache = false;

        public Tabweb(System.Windows.Forms.TabControl c, TabPage pestaña)
        {
           
            InitializeComponent();

            this.Height = c.Height;
            this.Width = c.Width;
            pestaña.Controls.Add(this);
            c.Controls.Add(pestaña);
        }
        public void cargarPestanna()
        {
            webBrow.Navigate("www.google.com");
            webBrow.Navigated += new WebBrowserNavigatedEventHandler(NavigatedMethod);
            webBrow.Navigating += new WebBrowserNavigatingEventHandler(NavigatingMethod);
        }

        private void txtBuscar_Click(object sender, EventArgs e)
        {
           
        }
        private void Navegar(string NombreBusqueda)
        {

            if (String.IsNullOrEmpty(NombreBusqueda)) return;
            if (NombreBusqueda.Equals("about:blank")) return;
            if (!NombreBusqueda.StartsWith("http://") && !NombreBusqueda.StartsWith("https://"))
            {
                NombreBusqueda = "http://" + NombreBusqueda;
            }
            try
            {
                webBrow.Navigate(new Uri(NombreBusqueda));


            }
            catch (System.UriFormatException)
            {
                return;
            }

        }

        private void NavigatedMethod(object sender, WebBrowserNavigatedEventArgs e)
        {
            txtBuscar.Text = webBrow.Url.ToString();
            if(webBrow.ReadyState == WebBrowserReadyState.Complete)
            {
                if (!FromCache)
                {
                    CacheDR.Instance.add(webBrow.Url.ToString(), webBrow.DocumentText);
                }
            }
        }

        private void NavigatingMethod(object sender, WebBrowserNavigatingEventArgs e)
        {
            string html = CacheDR.Instance.BuscarEnCache(e.Url.ToString());
            if(html != null)
            {
                FromCache = true;
            }
            else
            {
                FromCache = false;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Navegar(txtBuscar.Text);
        }

        private void txtBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Navegar(txtBusqueda.Text);
            }
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            webBrow.GoBack();
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            webBrow.GoForward();
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            webBrow.Refresh();
        }
    }
}
