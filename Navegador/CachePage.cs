﻿
namespace Navegador
{
    class CachePage
    {
        private int edad;
        private string html;
        private bool onlyInRAM;

        public string Html
        {
            get
            {
                return html;
            }
            set
            {
                html = value;
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                edad = value;
            }
        }

        public bool OnlyInRAM
        {
            get
            {
                return onlyInRAM;
            }
            set
            {
                onlyInRAM = value;
            }
        }


        public CachePage(string html, bool onlyInRAM)
        {
            this.edad = 0;
            this.html = html;
            this.onlyInRAM = onlyInRAM;
        }
    }
}
