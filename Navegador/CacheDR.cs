﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Navegador
{
    class CacheDR
    {
        private static readonly CacheDR INSTANCE = new CacheDR();
        private Dictionary<string, CachePage> ramCache;
        private bool enLinea = false;
        private int acumuladas;

        private CacheDR()
        {
            acumuladas = 0;
            if (!File.Exists("cache.drson"))
            {
                try
                {
                    File.Create("cache.drson");
                    File.WriteAllText("cache.drson", "😎page num = 0🙊");
                    enLinea = true;
                } catch (Exception ex)
                {
                    Console.WriteLine("NO SE PUDO CREAR ARCHIVO DE CACHÉ");
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                enLinea = true;
            }
            if (!Directory.Exists("CachePages"))
            {
                try
                {
                    Directory.CreateDirectory("CachePages");
                }
                catch (Exception)
                {
                    enLinea = false;
                }
            }
            ramCache = new Dictionary<string, CachePage>();
        }

        public static CacheDR Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        public string BuscarEnCache(string uri)
        {
            CachePage cache;
            ramCache.TryGetValue(uri, out cache);
            if(cache == null)
            {
                cache = BuscarEnDD(uri);
            }
            if(cache != null)
            {
                cache.Edad = 0;
                foreach (KeyValuePair<string, CachePage> tupla in ramCache)
                {
                    tupla.Value.Edad++;
                }
            }
            return cache == null ? null : cache.Html;
        }

        public void add(string uri, string html)
        {
           try
           {
                CachePage page = new CachePage(html, true);
                try
                {
                    ramCache.Add(uri, page);
                }
                catch (Exception)
                {

                }
                acumuladas++;
                if (acumuladas == 3)
                {
                   foreach(KeyValuePair<string, CachePage> tupla in ramCache)
                   {
                        if (tupla.Value.OnlyInRAM)
                        {
                            persist(tupla.Key, tupla.Value);
                            tupla.Value.OnlyInRAM = false;
                            acumuladas --;
                        }
                   }
                }
           }
           catch(Exception)
           {
                Console.WriteLine("NO SIRVIÓ ESTA PORQUERÍA, ME CAGO EN LA PXXX");
           }
        }

        private CachePage BuscarEnDD(string uri)
        {
            if (enLinea)
            {
                return new DRsonManager().GetPage(uri);
            }
            return null;
        }

        private void persist(string url, CachePage pagina)
        {
            new DRsonManager().SavePage(url, pagina);
        }
    }
}
